package com.example.testasignment.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testasignment.NavMainDirections
import com.example.testasignment.databinding.FragmentMainBinding
import com.example.testasignment.model.dto.Link
import com.example.testasignment.ui.adapter.LinksAdapter
import com.example.testasignment.ui.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment:Fragment(), LinksAdapter.LinkItemListener {

    val viewModel:MainViewModel by viewModels()
    lateinit var binding:FragmentMainBinding
    lateinit var adapter: LinksAdapter
    val args: MainFragmentArgs by navArgs()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentMainBinding.inflate(inflater,container,false)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        loadData()
        binding.refreshLayout.setOnRefreshListener {
            loadData()
        }

        viewModel.observeLinks().observe(viewLifecycleOwner){
            binding.refreshLayout.isRefreshing = false
            it?.let{
                adapter.updateItems(it)
            }
        }
    }

    private fun setupRecyclerView() {
        binding.rvLinks.layoutManager = LinearLayoutManager(requireContext())
        adapter = LinksAdapter(this)
        binding.rvLinks.adapter = adapter
    }

    private fun loadData(){
        viewModel.loadLinks()
    }

    override fun onItemClicked(link: Link) {
        val url = link.href.substringBefore("{")
        val action = NavMainDirections.actionFragmentContent(url,link.title)
        findNavController().navigate(action)
    }
}