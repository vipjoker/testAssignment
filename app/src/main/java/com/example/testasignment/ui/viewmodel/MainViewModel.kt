package com.example.testasignment.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testasignment.model.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(val mainRepository: MainRepository) : ViewModel() {

    fun loadPageContent(url: String) {
        viewModelScope.launch {
            mainRepository.loadPage(url)
        }
    }

    fun loadLinks()  {
        viewModelScope.launch{

            mainRepository.loadLinks()

        }
    }


    fun observeLinks() = mainRepository.observeLinks()

    fun observeContent(url:String ) = mainRepository.observePage(url)

}