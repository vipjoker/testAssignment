package com.example.testasignment.ui.actvity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.example.testasignment.C
import com.example.testasignment.R
import com.example.testasignment.ui.fragment.MainFragmentArgs
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)


        val fragment = supportFragmentManager.findFragmentById(R.id.navRootFragment) as NavHostFragment
        fragment.navController.setGraph( R.navigation.main_nav_graph,
            MainFragmentArgs(C.BASE_URL + "/androiddash-se").toBundle())

//            InnerFolderListFragmentArgs(null,args.folderType).toBundle())


    }
}