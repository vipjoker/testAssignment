package com.example.testasignment.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.testasignment.databinding.ItemLinkBinding
import com.example.testasignment.model.dto.Link



import android.view.View


import android.animation.ObjectAnimator
import android.view.animation.DecelerateInterpolator
import android.widget.ProgressBar
import com.example.testasignment.R
import com.example.testasignment.model.dto.Content

class PageContentAdapter (val listener:PageItemListener) : ListAdapter<Content, PageContentAdapter.ContentViewHolder>(DIFF_CALLBACK) {

        init {
            setHasStableIds(true)
        }

        private val items = mutableListOf<Content>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemLinkBinding.inflate(inflater,parent,false)
            return ContentViewHolder(binding)
        }

        override fun getItemId(position: Int): Long {
            return items[position].id.hashCode().toLong()
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {

            val item = items[position]
            holder.bind(item)

        }


        fun updateItems(it: List<Content>) {
            items.clear()
            items.addAll(it)
            notifyDataSetChanged()
        }

        inner class ContentViewHolder(val binding: ItemLinkBinding) : RecyclerView.ViewHolder(binding.root) {
            fun bind(item: Content){
                itemView.setOnClickListener {
                    listener.onItemClicked(item)
                }

                binding.tvTitle.text = item.title
                binding.tvDescription.text = item.type
            }
        }

        companion object {
            val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Content>() {

                override fun areItemsTheSame(oldItem: Content, newItem: Content): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(oldItem: Content, newItem: Content): Boolean {
                    return oldItem == newItem
                }

            }
        }

        interface PageItemListener{
            fun onItemClicked(link: Content)
        }
    }



