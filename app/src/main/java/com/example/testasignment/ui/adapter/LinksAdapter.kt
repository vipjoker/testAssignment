package com.example.testasignment.ui.adapter

import android.view.View
import com.example.testasignment.model.dto.Link


import android.animation.ObjectAnimator
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.ProgressBar
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.testasignment.R
import com.example.testasignment.databinding.ItemLinkBinding

class LinksAdapter(val listener:LinkItemListener) : ListAdapter<Link, LinksAdapter.LinkViewHolder>(DIFF_CALLBACK) {

    init {
        setHasStableIds(true)
    }

    private val items = mutableListOf<Link>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinkViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemLinkBinding.inflate(inflater,parent,false)
        return LinkViewHolder(binding)
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: LinkViewHolder, position: Int) {

        val item = items[position]
        holder.bind(item)

    }


    fun updateItems(it: List<Link>) {
        items.clear()
        items.addAll(it)
        notifyDataSetChanged()
    }

    inner class LinkViewHolder(val binding: ItemLinkBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item:Link){
            itemView.setOnClickListener {
                listener.onItemClicked(item)
            }

            binding.tvTitle.text = item.title
            binding.tvDescription.text = item.name
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Link>() {

            override fun areItemsTheSame(oldItem: Link, newItem: Link): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Link, newItem: Link): Boolean {
                return oldItem == newItem
            }

        }
    }

    interface LinkItemListener{
        fun onItemClicked(link:Link)
    }
}

