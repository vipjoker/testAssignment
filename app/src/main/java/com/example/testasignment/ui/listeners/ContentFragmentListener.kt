package com.example.testasignment.ui.listeners

interface ContentFragmentListener {
    fun onBackClicked()
}