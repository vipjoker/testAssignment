package com.example.testasignment.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testasignment.NavMainDirections
import com.example.testasignment.databinding.FragmentContentBinding
import com.example.testasignment.databinding.FragmentMainBinding
import com.example.testasignment.model.dto.Content
import com.example.testasignment.model.dto.Link
import com.example.testasignment.ui.adapter.LinksAdapter
import com.example.testasignment.ui.adapter.PageContentAdapter
import com.example.testasignment.ui.listeners.ContentFragmentListener
import com.example.testasignment.ui.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContentFragment: Fragment() ,PageContentAdapter.PageItemListener,ContentFragmentListener {

        val viewModel: MainViewModel by viewModels()
        lateinit var binding: FragmentContentBinding
        lateinit var adapter: PageContentAdapter
        val args: ContentFragmentArgs by navArgs()
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            binding = FragmentContentBinding.inflate(inflater,container,false)
            binding.listener = this
            binding.title = args.title
            return binding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            setupRecyclerView()
            loadData()
            binding.refreshLayout.setOnRefreshListener {
                loadData()
            }

            viewModel.observeContent(args.url).observe(viewLifecycleOwner){
                binding.refreshLayout.isRefreshing = false
                it?.let{
                    adapter.updateItems(it)
                }
            }

        }

        private fun setupRecyclerView() {
            binding.rvLinks.layoutManager = LinearLayoutManager(requireContext())
            adapter = PageContentAdapter(this)
            binding.rvLinks.adapter = adapter
        }

        private fun loadData(){
           viewModel.loadPageContent(args.url)
        }

        override fun onItemClicked(link: Content) {
          Toast.makeText(requireContext(),"not implemented",Toast.LENGTH_SHORT).show()
        }

    override fun onBackClicked() {
        findNavController().navigateUp()
    }
}
