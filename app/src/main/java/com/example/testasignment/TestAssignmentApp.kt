package com.example.testasignment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestAssignmentApp:Application() {


}