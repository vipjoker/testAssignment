package com.example.testasignment.model.remote

import com.example.testasignment.model.api.ApiService
import com.example.testasignment.model.dto.Link
import com.example.testasignment.model.dto.Page
import java.lang.Exception

class DefaultMainRepositoryRemote(private val service: ApiService) :MainRepositoryRemote{

    override suspend fun getLinks(): List<Link> {
        val response = service.getRootLinks()
        val data = response.data
        return data?.links ?: listOf()
    }


    override suspend fun getPage(url: String): Page? {

            val data = service.getPage(url)
            return data.data
    }
}