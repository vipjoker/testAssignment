package com.example.testasignment.model.converter

import com.example.testasignment.model.dto.*
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class
LinkResponseAdapter : JsonDeserializer<ResponseWrapper<Links>> {

    override fun deserialize(json: JsonElement?,
                             typeOfT: Type?,
                             context: JsonDeserializationContext?): ResponseWrapper<Links> {

        try {
            val links = mutableListOf<Link>()
            val linksArray = json?.asJsonObject
                ?.getAsJsonObject("_links")
                ?.getAsJsonArray("viaplay:sections")

            linksArray?.forEach {
                val jsonObject = it.asJsonObject
                val link = Link(
                    jsonObject["id"].asString,
                    jsonObject["title"].asString,
                    jsonObject["href"].asString,
                    jsonObject["type"].asString,
                    jsonObject["name"].asString,
                    jsonObject["templated"].asBoolean)
                links.add(link)
            }

          return ResponseWrapper(Links(links),null)

        } catch (e: Exception) {
            e.printStackTrace()
            return ResponseWrapper(null, Error("Failed to parse response: " + e.message))
        }
    }


    companion object {
        private val type = object : TypeToken<ResponseWrapper<Links>>() {}.type

        fun register(builder: GsonBuilder) {
            builder.registerTypeAdapter(type, LinkResponseAdapter())
        }
    }
}