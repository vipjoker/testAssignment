package com.example.testasignment.model.converter

import com.example.testasignment.model.dto.*
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class PageResponseAdapter : JsonDeserializer<ResponseWrapper<Page>> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): ResponseWrapper<Page> {

        try {
            val contents = mutableListOf<Content>()
            val linksArray = json?.asJsonObject
                ?.getAsJsonObject("_embedded")
                ?.getAsJsonArray("viaplay:blocks")

            linksArray?.forEach {
                val jsonObject = it.asJsonObject
                val content = Content(
                    jsonObject["id"].asString,
                    jsonObject["title"].asString,
                    jsonObject["type"].asString,
                )
                contents.add(content)
            }

            val page = Page()
            page.content.addAll(contents)
//            page.link = Links(links)
            return ResponseWrapper(page, null)

        } catch (e: Exception) {
            e.printStackTrace()
            return ResponseWrapper(null, Error("Failed to parse response: " + e.message))
        }
    }


    companion object {
        private val type = object : TypeToken<ResponseWrapper<Page>>() {}.type

        fun register(builder: GsonBuilder) {
            builder.registerTypeAdapter(type, PageResponseAdapter())
        }
    }
}