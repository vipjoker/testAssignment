package com.example.testasignment.model.api

import com.example.testasignment.model.dto.Links
import com.example.testasignment.model.dto.Page
import com.example.testasignment.model.dto.ResponseWrapper
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiService {

    @GET
    suspend fun  getPage(@Url url:String): ResponseWrapper<Page>

    @GET("/androiddash-se")
    suspend fun  getRootLinks(): ResponseWrapper<Links>
}