package com.example.testasignment.model.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "content")
data class Content (
    @PrimaryKey
    val id:String,
    val title:String,
    val type: String,
    var url:String? = null
)
