package com.example.testasignment.model.local

import androidx.lifecycle.LiveData
import com.example.testasignment.model.db.ContentDao
import com.example.testasignment.model.dto.Content
import com.example.testasignment.model.dto.Link

class DefaultMainRepositoryLocal(val dao:ContentDao):MainRepositoryLocal {



    override fun observeLinks(): LiveData<List<Link>> {
       return dao.getLinks()
    }

    override fun observeContent(url:String): LiveData<List<Content>> {
            return dao.getContent(url)
    }

    override suspend fun saveLinks(links: List<Link>) {
        dao.saveLinks(links)
    }

    override suspend fun saveContents(contents: List<Content>) {
        dao.saveContents(contents)
    }
}