package com.example.testasignment.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.testasignment.model.dto.Content
import com.example.testasignment.model.dto.Link


@Database(entities = [
    Content::class,
    Link::class],
    version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getContentDao():ContentDao

}
