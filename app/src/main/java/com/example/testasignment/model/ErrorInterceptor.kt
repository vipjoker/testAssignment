package com.example.testasignment.model

import okhttp3.*


class ErrorInterceptor() : Interceptor {



        override fun intercept(chain: Interceptor.Chain): Response {
            val request: Request = chain.request()
            return try {
                val response = chain.proceed(request)
                return response
            } catch (e: Exception) {
                e.printStackTrace()
                 val json = MediaType.parse("application/json")
                val body = ResponseBody.create(json,"{}")

               val builder =  Response.Builder()
                builder.request(request)
                builder.body(body)
                builder.code(200)
                builder.protocol(Protocol.HTTP_1_1)
                builder.message("")
                return builder.build()

            }
        }

    }