package com.example.testasignment.model.remote

import com.example.testasignment.model.dto.Link
import com.example.testasignment.model.dto.Page

interface MainRepositoryRemote {
    suspend fun getLinks():List<Link>

    suspend fun getPage(url: String): Page?
}