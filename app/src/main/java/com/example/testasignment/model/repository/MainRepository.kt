package com.example.testasignment.model.repository

import androidx.lifecycle.LiveData
import com.example.testasignment.model.dto.Content
import com.example.testasignment.model.dto.Link
import com.example.testasignment.model.dto.Page

interface MainRepository {
    suspend fun loadLinks()
    suspend fun loadPage(url: String)

    fun observeLinks():LiveData<List<Link>>
    fun observePage(url: String):LiveData <List<Content>>
}