package com.example.testasignment.model.dto

class Page {
    var link:Link? = null
    var content :MutableList<Content> = mutableListOf()
}