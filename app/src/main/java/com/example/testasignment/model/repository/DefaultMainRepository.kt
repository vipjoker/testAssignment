package com.example.testasignment.model.repository

import androidx.lifecycle.LiveData
import com.example.testasignment.model.dto.Content
import com.example.testasignment.model.dto.Link
import com.example.testasignment.model.dto.Page
import com.example.testasignment.model.local.MainRepositoryLocal
import com.example.testasignment.model.remote.MainRepositoryRemote

class DefaultMainRepository(
    val remoteRepository: MainRepositoryRemote,
    val localRepository: MainRepositoryLocal
) : MainRepository {

    override suspend fun loadLinks() {

        val links =  remoteRepository.getLinks()
        localRepository.saveLinks(links)

    }


    override suspend fun loadPage(url: String) {
        val page =  remoteRepository.getPage(url)
        val contentList = page?.content
        contentList?.let{ list->
            list.forEach {
                it.url = url
            }
            localRepository.saveContents(list)
        }

    }


    override fun observeLinks(): LiveData<List<Link>> {
        return localRepository.observeLinks()
    }

    override fun observePage(url: String): LiveData<List<Content>> {
        return localRepository.observeContent(url)
    }
}