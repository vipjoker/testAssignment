package com.example.testasignment.model.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.testasignment.model.dto.Content
import com.example.testasignment.model.dto.Link

@Dao
interface ContentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveContents(contents: List<Content>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveLinks(contents: List<Link>)

    @Query("SELECT * FROM link")
    fun getLinks(): LiveData<List<Link>>

    @Query("SELECT * FROM content WHERE url == :url")
    fun getContent(url:String): LiveData<List<Content>>
}