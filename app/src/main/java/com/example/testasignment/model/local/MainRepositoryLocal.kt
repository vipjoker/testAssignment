package com.example.testasignment.model.local

import androidx.lifecycle.LiveData
import com.example.testasignment.model.dto.Content
import com.example.testasignment.model.dto.Link

interface MainRepositoryLocal {

    fun observeLinks():LiveData<List<Link>>

    fun observeContent(url:String):LiveData<List<Content>>

    suspend fun saveLinks(links:List<Link>)

    suspend fun saveContents(contents:List<Content>)


}