package com.example.testasignment.model.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "link")
data class Link(
    @PrimaryKey
    val id: String,
    val title: String,
    val href: String,
    val type: String,
    val name: String,
    val templated: Boolean
)