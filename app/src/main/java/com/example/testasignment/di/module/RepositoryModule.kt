package com.example.testasignment.di.module

import android.content.Context
import androidx.room.Room
import com.example.testasignment.C
import com.example.testasignment.model.api.ApiService
import com.example.testasignment.model.db.AppDatabase
import com.example.testasignment.model.db.ContentDao
import com.example.testasignment.model.local.DefaultMainRepositoryLocal
import com.example.testasignment.model.local.MainRepositoryLocal
import com.example.testasignment.model.remote.DefaultMainRepositoryRemote
import com.example.testasignment.model.remote.MainRepositoryRemote
import com.example.testasignment.model.repository.DefaultMainRepository
import com.example.testasignment.model.repository.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRemoteRepository(apiService: ApiService):MainRepositoryRemote {
        return DefaultMainRepositoryRemote(apiService)
    }

    @Provides
    @Singleton
    fun provideRepository(remoteRepository: MainRepositoryRemote,localRepository:MainRepositoryLocal):MainRepository {
        return DefaultMainRepository(remoteRepository,localRepository)
    }

    @Provides
    @Singleton
    fun provideLocalRepository(dao:ContentDao):MainRepositoryLocal {
        return DefaultMainRepositoryLocal(dao)
    }

    @Provides
    @Singleton
    fun providesAppDatabase(@ApplicationContext applicationContext: Context): AppDatabase {
        return Room.databaseBuilder(applicationContext, AppDatabase::class.java, C.APP_DATABASE)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideContentsDao(appDatabase:AppDatabase):ContentDao{
        return appDatabase.getContentDao()
    }

}